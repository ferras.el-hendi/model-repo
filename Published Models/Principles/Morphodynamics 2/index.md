---
MorpheusModelID: M2052

authors: [N. Mulberry, L. Edelstein-Keshet]
contributors: [N. Mulberry]

published_model: original

title: "Three Layer Circuit"
date: "2021-03-24T04:40:00+01:00"
lastmod: "2021-07-26T19:43:00+02:00"

tags:
- Cadherin-1
- CDH1
- Cell Sorting
- Cellular Potts Model
- CPM
- Differential Adhesion
- E-cadherin
- Multiscale Models
- Notch Signaling Pathway
- Three-layer Structure

categories:
- DOI:10.1088/1478-3975/abb2dc
---
## Introduction

This model combines simple Notch signaling with the cellular Potts model (CPM), which drives cell sorting as a result of differential adhesion. Model details are contained in the reference [Mulberry and Keshet, 2020](#reference).

This experiment adds to the setup of the [Two Layer Circuit](https://identifiers.org/morpheus/M2051) by allowing both A and B type cells to act as senders or receivers. The cells eventually self-organize into a stable three-layer structure.  

## Description

We define two cells types, each with sender and receiver capabilities, although for two different signaling pathways.  
  
![](schematic_threelayer.png)

We have defined two ligand–receptor pairs, D–N and G–R (see schematic diagram above). The A-type cell expresses D, which activates neighboring N on B-type cells. Activation induces the B-type cells to express E-cadhi, and ligand G. The G ligand can subsequently activate neighboring R on A-type cells, which induces expression of E-cad at low levels (E-cadlo). The model equations for the A-type cells (encoding D ligand and R receptor) are

$$\begin{align}
\frac{\mathrm dR}{\mathrm dt} &= R_0 \left(1 + \frac{I^p}{I_0^p+I^p}\right) - \kappa_tRG_\text{ext} - \gamma R \\\\
\frac{\mathrm dI}{\mathrm dt} &= \kappa_tRT_\text{ext}- \gamma_I I \\\\
\frac{\mathrm dE^\text{lo}}{\mathrm dt} &= E_0^\text{lo} \frac{I^p}{I_0^p+I^p} - \gamma E^\text{lo} \\\\
\frac{\mathrm dD}{\mathrm dt} &= D_0 - \kappa_tN_\text{ext}D - \gamma D \\\\
\end{align}$$

To account for a lower maximal level of E-cad in activated A-type cells than for B-type cells, we scale $E^\text{lo} = E/3$.

The corresponding equations for the B-type cells (encoding G ligand and N receptor) are

$$\begin{align}
\frac{\mathrm dN}{\mathrm dt} &= N_0 \left(1 + \frac{I^p}{I_0^p+I^p}\right) - \kappa_tND_\text{ext}-\gamma N \\\\
\frac{\mathrm dI}{\mathrm dt} &= \kappa_tND_\text{ext}- \gamma_I I\\\\
\frac{\mathrm dE^\text{hi}}{\mathrm dt} &= E^\text{hi}_0 \frac{I^p}{I_0^p+I^p} - \gamma E^\text{hi}\\\\
\frac{\mathrm dG}{\mathrm dt} &= D_0 - \kappa_tR_\text{ext}G - \gamma G \\\\
\end{align}$$

## Results

![](ThreeLayerCircuit_Mov7.gif "Three Layer Circuit")

## Reference

This model is described in the peer-reviewed publication:

>N. Mulberry, L. Edelstein-Keshet: [Self-organized Multicellular Structures from Simple Cell Signaling: A Computational Model][reference]. *Phys. Biol.* **17**: 066003, 2020.

Our computational model was inspired by the experimental work of [Toda et al., 2018](https://science.sciencemag.org/content/361/6398/156). The Notch signaling model was based on that of [Boareto et al., 2015](https://www.pnas.org/content/112/5/E402.short). 

[reference]: https://doi.org/10.1088/1478-3975/abb2dc