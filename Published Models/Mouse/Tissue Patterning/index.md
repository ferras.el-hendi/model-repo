---
MorpheusModelID: M6749

authors: [M. Kücken, L. Brusch]
contributors: [M. Kücken]

title: Liver Zonation
date: "2019-12-24T00:00:00+01:00"
lastmod: "2020-11-10T12:14:00+01:00"
---

>Mutual Zonated Interactions of Wnt and Hh Signaling Are Orchestrating the Metabolism of the Adult Liver in Mice and Human

## Reference

This model is described in the peer-reviewed publication:

>E. Kolbe, S. Aleithe, C. Rennert, L. Spormann, F. Ott, D. Meierhofer, R. Gajowski, C. Stöpel, S. Hoehme, M. Kücken, L. Brusch, M. Seifert, W. von Schoenfels, C. Schafmayer, M. Brosch, U. Hofmann, G. Damm, D. Seehofer, J. Hampe, R. Gebhardt, M. Matz-Soja: [Mutual Zonated Interactions of Wnt and Hh Signaling Are Orchestrating the Metabolism of the Adult Liver in Mice and Human][reference]. *Cell Rep.* **29**, 4553–4567, 2019.

[reference]: https://doi.org/10.1016/j.celrep.2019.11.104