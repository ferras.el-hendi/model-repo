---
MorpheusModelID: M0035

title: "MembraneProperties: Cell Polarization and Chemotaxis"
date: "2019-11-11T16:59:00+01:00"
lastmod: "2020-10-30T13:36:00+01:00"

aliases: [/examples/membraneproperties-cell-polarization-and-chemotaxis/]

menu:
  Built-in Examples:
    parent: Multiscale Models
    weight: 50
weight: 210
---
## Introduction

This model of cell polarity shows the coupling of three model formalisms:

- A cellular Potts model,
- a PDE model, solved on the membrane of the cell and
- an external gradient.

The cell membrane polarizes in response to the external gradient. Chemotactic cell motility depends on the polarity of the cell and the external gradient.  

![](cellpolarity0060.png "Cell dynamically repolarizes in response to switching external gradient.")

## Description

This example implements two models of cell polarity: Meinhardt's substrate-depletion model and Edelstein-Keshet's wave-pinning model. The user can switch polarity model by ```Disabling```/```Enabling``` the relevant ```System```.

The model defines a one-dimensional reaction-diffusion system (```MembraneProperty```) representing membrane-bound molecules, and is mapped to a cellular Potts model defining a discrete shaped cell. An external gradient, specified in a ```PDE```, provides a signal for the polarization of the cell. In turn, the polarity of the cell influences its ```chemotaxic``` behavior. 

After a switch in direction of the gradient, the cell re-polarizes in the new direction and starts to move up the gradient, if the wave pinning model has been selected.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/60636346?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>