---
MorpheusModelID: M0023

authors: [A. Czirók, K. Varga, E. Méhes, A. Szabó]

title: "Persistence"
date: "2019-11-06T17:09:00+01:00"
lastmod: "2020-10-30T12:41:00+01:00"

aliases: [/examples/persistence/]

menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 30
weight: 130
---

## Introduction

This example shows self-organized collective motion of cells as a result of persistence ('cellular inertia'). A similar model has recently been used by [Czirók *et al.* (2013)][czirok-2013].

![](persistence.png "Persistence of individual cells causes self-organized collective motion.")

## Description

The model uses the ```Persistence``` plugin that causes cells to prefer to move in their current direction. The direction is stored in a ```PropertyVector``` that is used to plot the color and arrows in ```Gnuplotter```.

The model is simulated in a circular domain with constant boundary conditinos, which can be set up in ```Lattice/Domain/Circle```. The value for the constant boundary is specified in ```CPM/BoundaryValue```.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/50699261?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Change the boundary conditions from ```circular```$\ = \ $```constant``` to $x/y = \ $```periodic``` and observe the resulting collective motion.
- Change the ```decay-time``` of ```Persistence``` (specifying the 'memory').

## Reference

A. Czirók, K. Varga, E. Méhes, A. Szabó: [Collective cell streams in epithelial monolayers depend on cell adhesion][czirok-2013]. *New J. Phys.* **15** (7): 075006, 2013.

[czirok-2013]: http://dx.doi.org/10.1088/1367-2630/15/7/075006